use std::future::Future;

use anyhow::Result;
use tokio::io::{AsyncReadExt, AsyncWriteExt};

use crate::types::{Read, Write};

pub trait Packet {
    fn read<T: AsyncReadExt + Send + Unpin + Read, U: Packet + Sized>(stream: &mut T) -> impl Future<Output = Result<U>> + Send;
    fn write<T: AsyncWriteExt + Send + Unpin + Write + Sized>(self, stream: &mut T) -> impl Future<Output = Result<()>> + Send;
}
