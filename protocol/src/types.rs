use std::{future::Future, io::Cursor, str::from_utf8};

use anyhow::{anyhow, Result};
use tokio::io::{AsyncReadExt, AsyncWriteExt};

const SEGMENT_BITS: u8 = 0x7F;
const CONTINUE_BIT: u8 = 0x80;

#[derive(Debug)]
pub struct UUID {
    pub msb: u64,
    pub lsb: u64,
}

#[derive(Debug)]
pub struct Identifier {
    pub namespace: String,
    pub value: String,
}

#[derive(Debug)]
pub struct Position {
    pub x: i32,
    pub y: i16,
    pub z: i32,
}

pub trait Read {
    fn p_read_frame(&mut self) -> impl Future<Output = Result<(i32, Cursor<Vec<u8>>)>> + Send;
    fn p_read_bool(&mut self) -> impl Future<Output = Result<(bool, i32)>> + Send;
    fn p_read_u8(&mut self) -> impl Future<Output = Result<(u8, i32)>> + Send;
    fn p_read_i8(&mut self) -> impl Future<Output = Result<(i8, i32)>> + Send;
    fn p_read_u16(&mut self) -> impl Future<Output = Result<(u16, i32)>> + Send;
    fn p_read_i16(&mut self) -> impl Future<Output = Result<(i16, i32)>> + Send;
    fn p_read_i32(&mut self) -> impl Future<Output = Result<(i32, i32)>> + Send;
    fn p_read_var_int(&mut self) -> impl Future<Output = Result<(i32, i32)>> + Send;
    fn p_read_u64(&mut self) -> impl Future<Output = Result<(u64, i32)>> + Send;
    fn p_read_i64(&mut self) -> impl Future<Output = Result<(i64, i32)>> + Send;
    fn p_read_var_long(&mut self) -> impl Future<Output = Result<(i64, i32)>> + Send;
    fn p_read_f32(&mut self) -> impl Future<Output = Result<(f32, i32)>> + Send;
    fn p_read_f64(&mut self) -> impl Future<Output = Result<(f64, i32)>> + Send;
    fn p_read_string(&mut self) -> impl Future<Output = Result<(String, i32)>> + Send;
    fn p_read_byte_array(&mut self) -> impl Future<Output = Result<(Vec<u8>, i32)>> + Send;
    fn p_read_uuid(&mut self) -> impl Future<Output = Result<(UUID, i32)>> + Send;
    fn p_read_identifier(&mut self) -> impl Future<Output = Result<(Identifier, i32)>> + Send;
    fn p_read_position(&mut self) -> impl Future<Output = Result<(Position, i32)>> + Send;
}

pub trait Write {
    fn p_write<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> impl Future<Output = Result<()>> + Send;
}
pub trait WriteVarInt {
    fn p_write_var_int<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> impl Future<Output = Result<()>> + Send;
}
pub trait WriteVarLong {
    fn p_write_var_long<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> impl Future<Output = Result<()>> + Send;
}

impl<T: AsyncReadExt + Send + Unpin> Read for T {
    async fn p_read_frame(&mut self) -> Result<(i32, Cursor<Vec<u8>>)> {
        let (len, _) = self.p_read_var_int().await?;
        let mut data = vec![0; len as usize];
        self.read_exact(data.as_mut()).await?;
        let mut data = Cursor::new(data);
        let (id, _) = data.p_read_var_int().await?;
        let mut packet_data = Vec::new();
        data.read_to_end(packet_data.as_mut()).await?;
        Ok((id, Cursor::new(packet_data)))
    }
    async fn p_read_bool(&mut self) -> Result<(bool, i32)> {
        let mut buf: [u8; 1] = [0];
        self.read(buf.as_mut()).await?;
        Ok((buf[0] == 0x01, 1))
    }
    async fn p_read_u8(&mut self) -> Result<(u8, i32)> {
        let mut buf = [0_u8; 1];
        self.read(&mut buf).await?;
        Ok((buf[0], 1))
    }
    async fn p_read_i8(&mut self) -> Result<(i8, i32)> {
        let mut buf: [u8; 1] = [0];
        self.read(buf.as_mut()).await?;
        unsafe {
            Ok((std::mem::transmute::<u8, i8>(buf[0]), 1))
        }
    }
    async fn p_read_u16(&mut self) -> Result<(u16, i32)> {
        let mut buf = [0; 2];
        self.read(buf.as_mut()).await?;
        Ok(((buf[1] as u16) << 8 | buf[0] as u16, 2))
    }
    async fn p_read_i16(&mut self) -> Result<(i16, i32)> {
        let mut buf = [0; 2];
        self.read(buf.as_mut()).await?;
        unsafe {
            let data = (buf[1] as u16) << 8 | buf[0] as u16;
            Ok((std::mem::transmute::<u16, i16>(data), 2))
        }
    }
    async fn p_read_i32(&mut self) -> Result<(i32, i32)> {
        let mut buf = [0; 4];
        self.read(buf.as_mut()).await?;
        unsafe {
            let data = (buf[3] as u32) << 24 | (buf[2] as u32) << 16
                        | (buf[1] as u32) << 8 | buf[0] as u32;
            Ok((std::mem::transmute::<u32, i32>(data), 4))
        }
    }
    async fn p_read_var_int(&mut self) -> Result<(i32, i32)> {
        let mut value: i32 = 0;
        let mut position = 0;
        let mut byte = [0_u8; 1];
        let mut len = 0;

        loop {
            len += 1;
            self.read(&mut byte).await?;
            value |= (byte[0] as i32 & 0b0111_1111 as i32) << position;

            if (byte[0] & 0b1000_0000) == 0 { break }

            position += 7;

            if position >= 32 { return Err(anyhow!("VarInt is too big")); }
        }
        Ok((value, len))
    }
    async fn p_read_u64(&mut self) -> Result<(u64, i32)> {
        let mut buf = [0; 8];
        self.read(buf.as_mut()).await?;
        unsafe {
            let data = (buf[7] as u64) << 56 | (buf[6] as u64) << 48
                        | (buf[5] as u64) << 40 | (buf[4] as u64) << 32 
                        | (buf[3] as u64) << 24 | (buf[2] as u64) << 16
                        | (buf[1] as u64) << 8 | buf[0] as u64;
            Ok((data, 8))
        }
    }
    async fn p_read_i64(&mut self) -> Result<(i64, i32)> {
        let mut buf = [0; 8];
        self.read(buf.as_mut()).await?;
        unsafe {
            let data = (buf[7] as u64) << 56 | (buf[6] as u64) << 48
                        | (buf[5] as u64) << 40 | (buf[4] as u64) << 32 
                        | (buf[3] as u64) << 24 | (buf[2] as u64) << 16
                        | (buf[1] as u64) << 8 | buf[0] as u64;
            Ok((std::mem::transmute::<u64, i64>(data), 8))
        }
    }
    async fn p_read_var_long(&mut self) -> Result<(i64, i32)> {
        let mut value: i64 = 0;
        let mut position = 0;
        let mut byte;
        let mut len = 0;

        loop {
            len += 1;
            byte = self.read_u8().await?;
            value |= (byte as i64 & SEGMENT_BITS as i64) << position;

            if (byte & CONTINUE_BIT) == 0 { break }

            position += 7;

            if position >= 64 { return Err(anyhow!("VarInt is too big")); }
        }
        Ok((value, len))
    }
    async fn p_read_f32(&mut self) -> Result<(f32, i32)> {
        let mut buf = Vec::new();
        self.read(buf.as_mut()).await?;
        unsafe {
            let data = (buf[3] as u32) << 24 | (buf[2] as u32) << 16
                        | (buf[1] as u32) << 8 | buf[0] as u32;
            Ok((std::mem::transmute::<u32, f32>(data), 4))
        }
    }
    async fn p_read_f64(&mut self) -> Result<(f64, i32)> {
        let mut buf = [0; 8];
        self.read_exact(buf.as_mut()).await?;
        unsafe {
            let data = (buf[7] as u64) << 56 | (buf[6] as u64) << 48
                        | (buf[5] as u64) << 40 | (buf[4] as u64) << 32 
                        | (buf[3] as u64) << 24 | (buf[2] as u64) << 16
                        | (buf[1] as u64) << 8 | buf[0] as u64;
            Ok((std::mem::transmute::<u64, f64>(data), 8))
        }
    }
    async fn p_read_string(&mut self) -> Result<(String, i32)> {
        let (len, len_len) = self.p_read_var_int().await?;
        let mut buf = vec![0; len as usize];
        self.read_exact(buf.as_mut()).await?;
        Ok((from_utf8(&buf)?.to_string(), len + len_len))
    }
    async fn p_read_byte_array(&mut self) -> Result<(Vec<u8>, i32)> {
        let (len, len_len) = self.p_read_var_int().await?;
        let mut buf = vec![0; len as usize];
        self.read_exact(buf.as_mut()).await?;
        Ok((buf, len + len_len))
    }
    async fn p_read_uuid(&mut self) -> Result<(UUID, i32)> {
        let (msb, _) = self.p_read_u64().await?;
        let (lsb, _) = self.p_read_u64().await?;
        Ok((UUID { msb, lsb }, 16))
    }
    async fn p_read_identifier(&mut self) -> Result<(Identifier, i32)> {
        let (data, len) = self.p_read_string().await?;
        let data: Vec<&str> = data.split(":").collect();
        Ok((Identifier { namespace: data[0].to_string(), value: data[1].to_string() }, len))
    }
    async fn p_read_position(&mut self) -> Result<(Position, i32)> {
        let (data, len) = self.p_read_i64().await?;
        let x = (data & (0x3FFFFFF << 38)) >> 38;
        let z = (data & (0x3FFFFFF << 12)) >> 12;
        let y = data & 0xFFF;
        Ok((Position { x: x as i32, y: y as i16, z: z as i32 }, len))
    }
}

impl Write for bool {
    async fn p_write<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Result<()>{
        let data = if self { 0x01 } else { 0x00 };
        stream.write(&[data]).await?;
        Ok(())
    }
}
impl Write for u8 {
    async fn p_write<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Result<()>{
        stream.write(&[self]).await?;
        Ok(())
    }
}
impl Write for i8 {
    async fn p_write<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Result<()>{
        unsafe {
            stream.write(&[std::mem::transmute(self)]).await?;
        }
        Ok(())
    }
}
impl Write for u16 {
    async fn p_write<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Result<()>{
        stream.write(&[(self>>8) as u8, (self & 0xFF) as u8]).await?;
        Ok(())
    }
}
impl Write for i16 {
    async fn p_write<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Result<()>{
        unsafe {
            let data = std::mem::transmute::<i16, u16>(self);
            stream.write(&[(data>>8) as u8, (data & 0xFF) as u8]).await?;
        }
        Ok(())
    }
}
impl Write for i32 {
    async fn p_write<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Result<()>{
        unsafe {
            let data = std::mem::transmute::<i32, u32>(self);
            stream.write(&[(data>>24 & 0xFF) as u8, (data>>16 & 0xFF) as u8,
                            (data>>8 & 0xFF) as u8, (data & 0xFF) as u8]).await?;
        }
        Ok(())
    }
}
impl WriteVarInt for i32 {
    async fn p_write_var_int<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Result<()>{
        let mut data = self;
        loop {
            if (data & !SEGMENT_BITS as i32) == 0 {
                stream.write_u8(data as u8).await?;
                return Ok(());
            }
            
            stream.write_u8(((data & SEGMENT_BITS as i32) | CONTINUE_BIT as i32) as u8).await?;

            data >>= 7;
        }
    }
}
impl Write for u64 {
    async fn p_write<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Result<()>{
        unsafe {
            let data = self;
            stream.write(&[(data>>56 & 0xFF) as u8, (data>>48 & 0xFF) as u8,
                            (data>>40 & 0xFF) as u8, (data>>32 & 0xFF) as u8,
                            (data>>24 & 0xFF) as u8, (data>>16 & 0xFF) as u8,
                            (data>>8 & 0xFF) as u8, (data & 0xFF) as u8]).await?;
        }
        Ok(())
    }
}
impl Write for i64 {
    async fn p_write<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Result<()>{
        unsafe {
            let data = std::mem::transmute::<i64, u64>(self);
            stream.write(&[(data>>56 & 0xFF) as u8, (data>>48 & 0xFF) as u8,
                            (data>>40 & 0xFF) as u8, (data>>32 & 0xFF) as u8,
                            (data>>24 & 0xFF) as u8, (data>>16 & 0xFF) as u8,
                            (data>>8 & 0xFF) as u8, (data & 0xFF) as u8]).await?;
        }
        Ok(())
    }
}
impl WriteVarLong for i64 {
    async fn p_write_var_long<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Result<()>{
        let mut data = self;
        loop {
            if (data & !SEGMENT_BITS as i64) == 0 {
                stream.write_u8(data as u8).await?;
                return Ok(());
            }
            
            stream.write_u8(((data & SEGMENT_BITS as i64) | CONTINUE_BIT as i64) as u8).await?;

            data >>= 7;
        }
    }
}
impl Write for f32 {
    async fn p_write<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Result<()>{
        unsafe {
            let data = std::mem::transmute::<f32, u32>(self);
            stream.write(&[(data>>24 & 0xFF) as u8, (data>>16 & 0xFF) as u8,
                            (data>>8 & 0xFF) as u8, (data & 0xFF) as u8]).await?;
        }
        Ok(())
    }
}
impl Write for f64 {
    async fn p_write<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Result<()>{
        unsafe {
            let data = std::mem::transmute::<f64, u64>(self);
            stream.write(&[(data>>56 & 0xFF) as u8, (data>>48 & 0xFF) as u8,
                            (data>>40 & 0xFF) as u8, (data>>32 & 0xFF) as u8,
                            (data>>24 & 0xFF) as u8, (data>>16 & 0xFF) as u8,
                            (data>>8 & 0xFF) as u8, (data & 0xFF) as u8]).await?;
        }
        Ok(())
    }
}
impl Write for String {
    async fn p_write<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Result<()>{
        (self.len() as i32).p_write_var_int(stream).await?;
        stream.write(self.as_bytes()).await?;
        Ok(())
    }
}
impl Write for &str {
    async fn p_write<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Result<()>{
        (self.len() as i32).p_write_var_int(stream).await?;
        stream.write(self.as_bytes()).await?;
        Ok(())
    }
}
impl Write for UUID {
    async fn p_write<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Result<()>{
        self.msb.p_write(stream).await?;
        self.lsb.p_write(stream).await?;
        Ok(())
    }
}
impl Write for Identifier {
    async fn p_write<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Result<()>{
        let data = format!("{}:{}", self.namespace, self.value);
        data.p_write(stream).await?;
        Ok(())
    }
}
impl Write for Position {
    async fn p_write<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Result<()>{
        let data = (self.x as i64) << 38 | (self.z as i64) << 12 | (self.y as i64);
        data.p_write(stream).await?;
        Ok(())
    }
}
impl Write for Vec<u8> {
    async fn p_write<U: AsyncWriteExt + Send + Unpin>(self, stream: &mut U) -> Result<()>{
        stream.write(self.as_ref()).await?;
        Ok(())
    }
}
impl Write for &[u8] {
    async fn p_write<U: AsyncWriteExt + Send + Unpin>(self, stream: &mut U) -> Result<()>{
        stream.write(self).await?;
        Ok(())
    }
}
