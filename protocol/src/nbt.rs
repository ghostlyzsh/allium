use std::{future::Future, pin::Pin};

use anyhow::Result;
use tokio::{io::{AsyncReadExt, AsyncWriteExt}, pin};

use crate::{types::{Read, Write}, utf::UtfString};

macro_rules! tag_match {
    ($name: ident, $data: ident, $stream: ident) => {
        {
            if let Some(name) = $name {
                (name.len() as u16).p_write($stream).await?;
                name.as_bytes().p_write($stream).await?;
            }
            $data.p_write($stream).await?;
        }
    }
}
#[derive(Debug, Clone)]
pub enum Tag {
    End,
    Byte(Option<String>, i8),
    Short(Option<String>, i16),
    Int(Option<String>, i32),
    Long(Option<String>, i64),
    Float(Option<String>, f32),
    Double(Option<String>, f64),
    ByteArray(Option<String>, Vec<i8>),
    Str(Option<String>, String),
    List(Option<String>, Vec<Tag>),
    Compound(Option<String>, Vec<Tag>),
    IntArray(Option<String>, Vec<i32>),
    LongArray(Option<String>, Vec<i64>),
}

impl Tag {
    pub fn type_id(&self) -> u8 {
        use Tag::*;
        match *self {
            End => 0,
            Byte(_, _) => 1,
            Short(_, _) => 2,
            Int(_, _) => 3,
            Long(_, _) => 4,
            Float(_, _) => 5,
            Double(_, _) => 6,
            ByteArray(_, _) => 7,
            Str(_, _) => 8,
            List(_, _) => 9,
            Compound(_, _) => 10,
            IntArray(_, _) => 11,
            LongArray(_, _) => 12,
        }
    }
    // read one tag at a time
    pub async fn read<T: AsyncReadExt + Send + Unpin + Read>(stream: &mut T) -> Result<Self> {
        let (type_id, _) = stream.p_read_u8().await?;
        let (len, _) = stream.p_read_u16().await?;
        let mut name = vec![0; len as usize];
        stream.read(name.as_mut()).await?;
        let name = UtfString::new(name);
        let tag = match type_id {
            0 => {
                Tag::End
            }
            1 => {
                let (data, _) = stream.p_read_i8().await?;
                Tag::Byte(Some(name.into()), data)
            }
            2 => {
                let (data, _) = stream.p_read_i16().await?;
                Tag::Short(Some(name.into()), data)
            }
            3 => {
                let (data, _) = stream.p_read_i32().await?;
                Tag::Int(Some(name.into()), data)
            }
            4 => {
                let (data, _) = stream.p_read_i64().await?;
                Tag::Long(Some(name.into()), data)
            }
            5 => {
                let (data, _) = stream.p_read_f32().await?;
                Tag::Float(Some(name.into()), data)
            }
            6 => {
                let (data, _) = stream.p_read_f64().await?;
                Tag::Double(Some(name.into()), data)
            }
            7 => {
                let (len, _) = stream.p_read_i32().await?;
                let mut data = vec![0; len as usize];
                stream.read(data.as_mut()).await?;
                let data = data.iter().map(|s| unsafe { std::mem::transmute(*s) }).collect();
                Tag::ByteArray(Some(name.into()), data)
            }
            8 => {
                let (len, _) = stream.p_read_u16().await?;
                let mut data = vec![0; len as usize];
                stream.read(data.as_mut()).await?;
                let string = UtfString::new(data);
                Tag::Str(Some(name.into()), string.into())
            }
            9 => {
                let (type_id, _) = stream.p_read_u8().await?;
                let (len, _) = stream.p_read_i32().await?;
                let mut tags = Vec::new();
                for _ in 0..len {
                    tags.push(Tag::nameless_read(type_id, stream).await?);
                }
                stream.p_read_u8().await?;
                Tag::List(Some(name.into()), tags)
            }
            10 => {
                let mut tags = Vec::new();
                loop {
                    let tag = Tag::read(stream).await?;
                    if let Tag::End = tag {
                        break;
                    }
                    tags.push(tag);
                }
                Tag::Compound(Some(name.into()), tags)
            }
            11 => {
                let (len, _) = stream.p_read_i32().await?;
                let mut ints = Vec::new();
                for _ in 0..len {
                    ints.push(stream.p_read_i32().await?.0);
                }
                Tag::IntArray(Some(name.into()), ints)
            }
            12 => {
                let (len, _) = stream.p_read_i32().await?;
                let mut longs = Vec::new();
                for _ in 0..len {
                    longs.push(stream.p_read_i64().await?.0);
                }
                Tag::LongArray(Some(name.into()), longs)
            }
            _ => panic!("Invalid Type Id received")
        };
        Ok(tag)
    }
    pub async fn nameless_read<T: AsyncReadExt + Send + Unpin + Read>(type_id: u8, stream: &mut T) -> Result<Self> {
        let tag = match type_id {
            1 => {
                let (data, _) = stream.p_read_i8().await?;
                Tag::Byte(None, data)
            }
            2 => {
                let (data, _) = stream.p_read_i16().await?;
                Tag::Short(None, data)
            }
            3 => {
                let (data, _) = stream.p_read_i32().await?;
                Tag::Int(None, data)
            }
            4 => {
                let (data, _) = stream.p_read_i64().await?;
                Tag::Long(None, data)
            }
            5 => {
                let (data, _) = stream.p_read_f32().await?;
                Tag::Float(None, data)
            }
            6 => {
                let (data, _) = stream.p_read_f64().await?;
                Tag::Double(None, data)
            }
            7 => {
                let (len, _) = stream.p_read_i32().await?;
                let mut data = vec![0; len as usize];
                stream.read(data.as_mut()).await?;
                let data = data.iter().map(|s| unsafe { std::mem::transmute(*s) }).collect();
                Tag::ByteArray(None, data)
            }
            8 => {
                let (len, _) = stream.p_read_u16().await?;
                let mut data = vec![0; len as usize];
                stream.read(data.as_mut()).await?;
                let string = UtfString::new(data);
                Tag::Str(None, string.into())
            }
            9 => {
                let (type_id, _) = stream.p_read_u8().await?;
                let (len, _) = stream.p_read_i32().await?;
                let mut tags = Vec::new();
                for _ in 0..len {
                    tags.push(Tag::nameless_read(type_id, stream).await?);
                }
                Tag::List(None, tags)
            }
            10 => {
                let mut tags = Vec::new();
                loop {
                    let tag = Tag::read(stream).await?;
                    if let Tag::End = tag {
                        break;
                    }
                    tags.push(tag);
                }
                Tag::Compound(None, tags)
            }
            11 => {
                let (len, _) = stream.p_read_i32().await?;
                let mut ints = Vec::new();
                for _ in 0..len {
                    ints.push(stream.p_read_i32().await?.0);
                }
                Tag::IntArray(None, ints)
            }
            12 => {
                let (len, _) = stream.p_read_i32().await?;
                let mut longs = Vec::new();
                for _ in 0..len {
                    longs.push(stream.p_read_i64().await?.0);
                }
                Tag::LongArray(None, longs)
            }
            _ => panic!("Invalid Type Id received")
        };
        Ok(tag)
    }
    pub fn write<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Pin<Box<impl Future<Output = Result<()>> + Send + '_>> {
        use Tag::*;
        Box::pin(async move {
            match self {
                End => {},
                Byte(ref name, data) => {
                    self.type_id().p_write(stream).await?;
                    tag_match!(name, data, stream)
                },
                Short(ref name, data) => {
                    self.type_id().p_write(stream).await?;
                    tag_match!(name, data, stream);
                },
                Int(ref name, data) => {
                    self.type_id().p_write(stream).await?;
                    tag_match!(name, data, stream);
                },
                Long(ref name, data) => {
                    self.type_id().p_write(stream).await?;
                    tag_match!(name, data, stream);
                },
                Float(ref name, data) => {
                    self.type_id().p_write(stream).await?;
                    tag_match!(name, data, stream);
                }
                Double(ref name, data) => {
                    self.type_id().p_write(stream).await?;
                    tag_match!(name, data, stream);
                }
                Tag::ByteArray(ref name, ref data) => {
                    self.type_id().p_write(stream).await?;
                    if let Some(name) = name {
                        (name.len() as u16).p_write(stream).await?;
                        name.as_bytes().p_write(stream).await?;
                    }
                    for element in data {
                        element.p_write(stream).await?;
                    }
                },
                Tag::Str(ref name, ref data) => {
                    self.type_id().p_write(stream).await?;
                    if let Some(name) = name {
                        (name.len() as u16).p_write(stream).await?;
                        name.as_bytes().p_write(stream).await?;
                    }
                    let string: UtfString = data.clone().into();
                    (string.data.len() as u16).p_write(stream).await?;
                    string.data.p_write(stream).await?;
                },
                Tag::List(ref name, ref data) => {
                    self.type_id().p_write(stream).await?;
                    if let Some(name) = name {
                        (name.len() as u16).p_write(stream).await?;
                        name.as_bytes().p_write(stream).await?;
                    }
                    let type_id = if data.len() > 0 {
                        data[0].type_id()
                    } else {
                        0x00
                    };
                    type_id.p_write(stream).await?;
                    (data.len() as i32).p_write(stream).await?;
                    for element in data {
                        element.clone().nameless_write(stream).await?;
                    }
                    0x00_u8.p_write(stream).await?;
                },
                Tag::Compound(ref name, ref data) => {
                    self.type_id().p_write(stream).await?;
                    if let Some(name) = name {
                        (name.len() as u16).p_write(stream).await?;
                        name.as_bytes().p_write(stream).await?;
                    }
                    for element in data {
                        element.clone().write(stream).await?;
                    }
                    0x00_u8.p_write(stream).await?;
                },
                Tag::IntArray(ref name, ref data) => {
                    self.type_id().p_write(stream).await?;
                    if let Some(name) = name {
                        (name.len() as u16).p_write(stream).await?;
                        name.as_bytes().p_write(stream).await?;
                    }
                    (data.len() as i32).p_write(stream).await?;
                    for element in data {
                        element.p_write(stream).await?;
                    }
                },
                Tag::LongArray(ref name, ref data) => {
                    self.type_id().p_write(stream).await?;
                    if let Some(name) = name {
                        (name.len() as u16).p_write(stream).await?;
                        name.as_bytes().p_write(stream).await?;
                    }
                    (data.len() as i32).p_write(stream).await?;
                    for element in data {
                        element.p_write(stream).await?;
                    }
                },
            };
            Ok(())
        })
    }
    pub fn nameless_write<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Pin<Box<impl Future<Output = Result<()>> + Send + '_>> {
        Box::pin(async move {
            use Tag::*;
            match self {
                End => {},
                Byte(_, data) => data.p_write(stream).await?,
                Short(_, data) => data.p_write(stream).await?,
                Int(_, data) => data.p_write(stream).await?,
                Long(_, data) => data.p_write(stream).await?,
                Float(_, data) => data.p_write(stream).await?,
                Double(_, data) => data.p_write(stream).await?,
                ByteArray(_, data) => {
                    for element in data {
                        element.p_write(stream).await?;
                    }
                },
                Str(_, data) => {
                    let string: UtfString = data.into();
                    (string.data.len() as u16).p_write(stream).await?;
                    string.data.p_write(stream).await?;
                },
                List(_, data) => {
                    let type_id = if data.len() > 0 {
                        data[0].type_id()
                    } else {
                        0x00
                    };
                    type_id.p_write(stream).await?;
                    (data.len() as i32).p_write(stream).await?;
                    for element in data {
                        element.nameless_write(stream).await?;
                    }
                    0x00_u8.p_write(stream).await?;
                },
                Compound(_, data) => {
                    for element in data {
                        element.write(stream).await?;
                    }
                    0x00_u8.p_write(stream).await?;
                },
                IntArray(_, data) => {
                    (data.len() as i32).p_write(stream).await?;
                    for element in data {
                        element.p_write(stream).await?;
                    }
                },
                LongArray(_, data) => {
                    (data.len() as i32).p_write(stream).await?;
                    for element in data {
                        element.p_write(stream).await?;
                    }
                },
            }
            Ok(())
        })
    }
}

