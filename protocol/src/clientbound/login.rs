use anyhow::Result;
use tokio::io::{AsyncReadExt, AsyncWriteExt};

use crate::types::{Identifier, Read, Write, WriteVarInt, UUID};

// id 0x00
#[derive(Debug)]
pub struct Disconnect {
    pub reason: String // JSON Text Component
}
impl Disconnect {
    pub async fn read<T: AsyncReadExt + Send + Unpin + Read>(stream: &mut T) -> Result<Self> {
        let (reason, _) = stream.p_read_string().await?;
        Ok(Disconnect { reason })
    }
    pub async fn write<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Result<()> {
        let mut data = Vec::new();
        0x00.p_write_var_int(&mut data).await?;
        self.reason.p_write(&mut data).await?;
        (data.len() as i32).p_write_var_int(stream).await?;
        data.p_write(stream).await?;
        Ok(())
    }
}

// id 0x01
#[derive(Debug)]
pub struct EncryptionRequest {
    pub server_id: String,
    pub pub_key: Vec<u8>,
    pub verify_token: Vec<u8>,
}
impl EncryptionRequest {
    pub async fn read<T: AsyncReadExt + Send + Unpin + Read>(stream: &mut T) -> Result<Self> {
        let (server_id, _) = stream.p_read_string().await?;
        let (pub_key, _) = stream.p_read_byte_array().await?;
        let (verify_token, _) = stream.p_read_byte_array().await?;
        Ok(EncryptionRequest { server_id, pub_key, verify_token })
    }
    pub async fn write<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Result<()> {
        let mut data = Vec::new();
        0x01.p_write_var_int(&mut data).await?;
        self.server_id.p_write(&mut data).await?;
        (self.pub_key.len() as i32).p_write_var_int(&mut data).await?;
        self.pub_key.p_write(&mut data).await?;
        (self.verify_token.len() as i32).p_write_var_int(&mut data).await?;
        self.verify_token.p_write(&mut data).await?;
        (data.len() as i32).p_write_var_int(stream).await?;
        data.p_write(stream).await?;
        Ok(())
    }
}

#[derive(Debug)]
pub struct Property {
    pub name: String,
    pub value: String,
    pub is_signed: bool,
    pub signature: Option<String>
}
impl Property {
    pub async fn read<T: AsyncReadExt + Send + Unpin + Read>(stream: &mut T) -> Result<Self> {
        let (name, _) = stream.p_read_string().await?;
        let (value, _) = stream.p_read_string().await?;
        let (is_signed, _) = stream.p_read_bool().await?;
        let mut signature = None;
        if is_signed {
            signature = Some(stream.p_read_string().await?.0);
        }
        Ok(Property { name, value, is_signed, signature })
    }
    pub async fn write<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Result<()> {
        self.name.p_write(stream).await?;
        self.value.p_write(stream).await?;
        self.is_signed.p_write(stream).await?;
        if self.is_signed {
            self.signature.unwrap().p_write(stream).await?;
        }
        Ok(())
    }
}

// id 0x02
#[derive(Debug)]
pub struct LoginSuccess {
    pub uuid: UUID,
    pub username: String,
    pub properties: Vec<Property>,
}
impl LoginSuccess {
    pub async fn read<T: AsyncReadExt + Send + Unpin + Read>(stream: &mut T) -> Result<Self> {
        let (uuid, _) = stream.p_read_uuid().await?;
        let (username, _) = stream.p_read_string().await?;
        let (num_properties, _) = stream.p_read_var_int().await?;
        let mut properties = Vec::new();
        for _ in 0..num_properties {
            properties.push(Property::read(stream).await?);
        }
        Ok(LoginSuccess { uuid, username, properties })
    }
    pub async fn write<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Result<()> {
        let mut data = Vec::new();
        0x02.p_write_var_int(&mut data).await?;
        self.uuid.p_write(&mut data).await?;
        self.username.p_write(&mut data).await?;
        (self.properties.len() as i32).p_write_var_int(&mut data).await?;
        for property in self.properties {
            property.write(&mut data).await?;
        }
        (data.len() as i32).p_write_var_int(stream).await?;
        data.p_write(stream).await?;
        Ok(())
    }
}

// id 0x03
#[derive(Debug)]
pub struct SetCompression {
    pub threshold: i32,
}
impl SetCompression {
    pub async fn read<T: AsyncReadExt + Send + Unpin + Read>(stream: &mut T) -> Result<Self> {
        let (threshold, _) = stream.p_read_var_int().await?;
        Ok(SetCompression { threshold })
    }
    pub async fn write<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Result<()> {
        let mut data = Vec::new();
        0x03.p_write_var_int(&mut data).await?;
        self.threshold.p_write_var_int(&mut data).await?;
        (data.len() as i32).p_write_var_int(stream).await?;
        data.p_write(stream).await?;
        Ok(())
    }
}

// id 0x04
#[derive(Debug)]
pub struct LoginPluginRequest {
    pub message_id: i32,
    pub channel: Identifier,
    pub data: Vec<u8>,
}
impl LoginPluginRequest {
    pub async fn read<T: AsyncReadExt + Send + Unpin + Read>(packet_len: i32, stream: &mut T) -> Result<Self> {
        let (message_id, m_id_len) = stream.p_read_var_int().await?;
        let (channel, channel_len) = stream.p_read_identifier().await?;
        let mut data = vec![0u8; (packet_len - m_id_len - channel_len) as usize];
        stream.read_exact(&mut data).await?;
        Ok(LoginPluginRequest { message_id, channel, data })
    }
    pub async fn write<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Result<()> {
        let mut data = Vec::new();
        0x04.p_write_var_int(&mut data).await?;
        self.message_id.p_write_var_int(&mut data).await?;
        self.channel.p_write(&mut data).await?;
        self.data.p_write(&mut data).await?;
        (data.len() as i32).p_write_var_int(stream).await?;
        data.p_write(stream).await?;
        Ok(())
    }
}
