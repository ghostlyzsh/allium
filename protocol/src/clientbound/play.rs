use anyhow::Result;
use tokio::io::{AsyncReadExt, AsyncWriteExt};

use crate::types::{Identifier, Position, Read, Write, WriteVarInt};

// id 0x20
pub struct GameEvent {
    pub event: u8,
    pub value: f32,
}
impl GameEvent {
    pub async fn read<T: AsyncReadExt + Send + Unpin + Read>(stream: &mut T) -> Result<Self> {
        let (event, _) = stream.p_read_u8().await?;
        let (value, _) = stream.p_read_f32().await?;
        Ok(GameEvent { event, value })
    }
    pub async fn write<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Result<()> {
        let mut data = Vec::new();
        0x20.p_write_var_int(&mut data).await?;

        self.event.p_write(stream).await?;
        self.value.p_write(stream).await?;

        (data.len() as i32).p_write_var_int(stream).await?;
        data.p_write(stream).await?;
        Ok(())
    }
}

// id 0x29
pub struct Login {
    pub entity_id: i32, // Int
    pub is_hardcore: bool,
    pub dimension_names: Vec<Identifier>,
    pub max_players: i32, // VarInt
    pub view_distance: i32, // VarInt
    pub simulation_distance: i32, // VarInt
    pub reduced_debug_info: bool,
    pub enable_respawn_screen: bool,
    pub do_limited_crafting: bool,
    pub dimension_type: Identifier,
    pub dimension_name: Identifier,
    pub hashed_seed: i64,
    pub game_mode: u8,
    pub previous_game_mode: i8,
    pub is_debug: bool,
    pub is_flat: bool,
    pub has_death_location: bool,
    pub death_dimension_name: Option<Identifier>,
    pub death_location: Option<Position>,
    pub portal_cooldown: i32, // VarInt
}
impl Login {
    pub async fn read<T: AsyncReadExt + Send + Unpin + Read>(stream: &mut T) -> Result<Self> {
        let (entity_id, _) = stream.p_read_i32().await?;
        let (is_hardcore, _) = stream.p_read_bool().await?;
        let (dimension_count, _) = stream.p_read_var_int().await?;
        let mut dimension_names = Vec::with_capacity(dimension_count as usize);
        for _ in 0..dimension_count {
            dimension_names.push(stream.p_read_identifier().await?.0);
        }
        let (max_players, _) = stream.p_read_var_int().await?;
        let (view_distance, _) = stream.p_read_var_int().await?;
        let (simulation_distance, _) = stream.p_read_var_int().await?;
        let (reduced_debug_info, _) = stream.p_read_bool().await?;
        let (enable_respawn_screen, _) = stream.p_read_bool().await?;
        let (do_limited_crafting, _) = stream.p_read_bool().await?;
        let (dimension_type, _) = stream.p_read_identifier().await?;
        let (dimension_name, _) = stream.p_read_identifier().await?;
        let (hashed_seed, _) = stream.p_read_i64().await?;
        let (game_mode, _) = stream.p_read_u8().await?;
        let (previous_game_mode, _) = stream.p_read_i8().await?;
        let (is_debug, _) = stream.p_read_bool().await?;
        let (is_flat, _) = stream.p_read_bool().await?;
        let (has_death_location, _) = stream.p_read_bool().await?;
        let mut death_dimension_name = None;
        let mut death_location = None;
        if has_death_location {
            death_dimension_name = Some(stream.p_read_identifier().await?.0);
            death_location = Some(stream.p_read_position().await?.0);
        }
        let (portal_cooldown, _) = stream.p_read_var_int().await?;
        Ok(Login {
            entity_id,
            is_hardcore,
            dimension_names,
            max_players,
            view_distance,
            simulation_distance,
            reduced_debug_info,
            enable_respawn_screen,
            do_limited_crafting,
            dimension_type,
            dimension_name,
            hashed_seed,
            game_mode,
            previous_game_mode,
            is_debug,
            is_flat,
            has_death_location,
            death_dimension_name,
            death_location,
            portal_cooldown,
        })
    }
    pub async fn write<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Result<()> {
        let mut data = Vec::new();
        0x29.p_write_var_int(&mut data).await?;

        self.entity_id.p_write(&mut data).await?;
        self.is_hardcore.p_write(&mut data).await?;
        (self.dimension_names.len() as i32).p_write_var_int(&mut data).await?;
        for dimension_name in self.dimension_names {
            dimension_name.p_write(&mut data).await?;
        }
        self.max_players.p_write_var_int(&mut data).await?;
        self.view_distance.p_write_var_int(&mut data).await?;
        self.simulation_distance.p_write_var_int(&mut data).await?;
        self.reduced_debug_info.p_write(&mut data).await?;
        self.enable_respawn_screen.p_write(&mut data).await?;
        self.do_limited_crafting.p_write(&mut data).await?;
        self.dimension_type.p_write(&mut data).await?;
        self.dimension_name.p_write(&mut data).await?;
        self.hashed_seed.p_write(&mut data).await?;
        self.game_mode.p_write(&mut data).await?;
        self.previous_game_mode.p_write(&mut data).await?;
        self.is_debug.p_write(&mut data).await?;
        self.is_flat.p_write(&mut data).await?;
        self.has_death_location.p_write(&mut data).await?;
        if self.has_death_location {
            self.death_dimension_name.unwrap().p_write(&mut data).await?;
            self.death_location.unwrap().p_write(&mut data).await?;
        }
        self.portal_cooldown.p_write_var_int(&mut data).await?;

        (data.len() as i32).p_write_var_int(stream).await?;
        data.p_write(stream).await?;
        Ok(())
    }
}
