use anyhow::Result;
use tokio::io::{AsyncReadExt, AsyncWriteExt};

use crate::{nbt::Tag, types::{Identifier, Read, Write, WriteVarInt, UUID}};

// id 0x00
pub struct ClientboundPluginMessage {
    pub channel: Identifier,
    pub data: Vec<u8>, // Byte Array
}

// id 0x01
pub struct Disconnect {
    pub reason: Tag, // Text Component
}

// id 0x02
pub struct FinishConfiguration {}
impl FinishConfiguration {
    pub async fn write<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Result<()> {
        let mut data = Vec::new();
        0x02.p_write_var_int(&mut data).await?;
        (data.len() as i32).p_write_var_int(stream).await?;
        data.p_write(stream).await?;
        Ok(())
    }
}

// id 0x03
pub struct ClientboundKeepAlive {
    pub keep_alive_id: i64,
}

// id 0x04
pub struct Ping {
    pub id: i32, // int
}

// id 0x05
pub struct RegistryData {
    pub registry_codec: Tag, // https://wiki.vg/Registry_Data
}
impl RegistryData {
    pub async fn read<T: AsyncReadExt + Send + Unpin + Read>(stream: &mut T) -> Result<Self> {
        let registry_codec = Tag::read(stream).await?;
        Ok(RegistryData { registry_codec })
    }
    pub async fn write<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Result<()> {
        let mut data = Vec::new();
        0x05.p_write_var_int(&mut data).await?;
        self.registry_codec.write(stream).await?;
        (data.len() as i32).p_write_var_int(stream).await?;
        data.p_write(stream).await?;
        Ok(())
    }
}

// id 0x06
pub struct RemoveResourcePack {
    pub has_uuid: bool,
    pub uuid: Option<UUID>,
}

// id 0x07
pub struct AddResourcePack {
    pub uuid: UUID,
    pub url: String,
    pub hash: String,
    pub forced: bool,
    pub has_prompt_message: bool,
    pub prompt_message: Option<Tag>, // Text Component
}

// id 0x08
pub struct FeatureFlags {
    pub total_features: i32, // VarInt
    pub features_flags: Vec<Identifier>,
}

// id 0x09
pub struct UpdateTags {
    pub array_len: i32, // VarInt
    pub tag_array: Vec<(Identifier, i32, i32)>
}
