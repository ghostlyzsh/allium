use anyhow::Result;
use tokio::io::{AsyncReadExt, AsyncWriteExt};

use crate::types::{Read, Write, WriteVarInt};

// id 0x00
pub struct StatusResponse {
    pub json_response: String,
}

// id 0x01
pub struct PingResponse {
    pub payload: i64,
}

impl StatusResponse {
    pub async fn read<T: AsyncReadExt + Send + Unpin + Read>(stream: &mut T) -> Result<Self> {
        let (json_response, _) = stream.p_read_string().await?;
        Ok(StatusResponse { json_response })
    }
    pub async fn write<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Result<()> {
        let mut data = Vec::new();
        0x00.p_write_var_int(&mut data).await?;
        self.json_response.p_write(&mut data).await?;
        (data.len() as i32).p_write_var_int(stream).await?;
        data.p_write(stream).await?;
        Ok(())
    }
}

impl PingResponse {
    pub async fn read<T: AsyncReadExt + Send + Unpin + Read>(stream: &mut T) -> Result<Self> {
        let (payload, _) = stream.p_read_i64().await?;
        Ok(PingResponse { payload })
    }
    pub async fn write<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Result<()> {
        let mut data = Vec::new();
        0x01.p_write_var_int(&mut data).await?;
        self.payload.p_write(&mut data).await?;
        (data.len() as i32).p_write_var_int(stream).await?;
        data.p_write(stream).await?;
        Ok(())
    }
}
