pub struct UtfString {
    pub data: Vec<u8>,
}

impl UtfString {
    pub fn new(data: Vec<u8>) -> Self {
        Self { data }
    }
}

impl From<String> for UtfString {
    fn from(value: String) -> Self {
        let mut data = Vec::new();
        for c in value.chars() {
            let point = c as u32;
            if point == 0 {
                data.push(0b1100_0000);
                data.push(0b1000_0000);
            } else if point <= 0x007F {
                data.push(point as u8);
            } else if point <= 0x07FF {
                data.push((0b1100_0000 | (0b0111_1100_0000 & point >> 5)) as u8);
                data.push((0b1000_0000 | (0b0000_0011_1111 & point)) as u8);
            } else if point <= 0xFFFF {
                data.push((0b1110_0000 | (0b1111_0000_0000_0000 & point >> 11)) as u8);
                data.push((0b1000_0000 | (0b0000_1111_1100_0000 & point >> 5)) as u8);
                data.push((0b1000_0000 | (0b0000_0000_0011_1111 & point)) as u8);
            }
        }
        UtfString { data }
    }
}

impl From<UtfString> for String {
    fn from(value: UtfString) -> Self {
        let mut result = String::new();
        for i in 0..value.data.len() {
            let c = value.data[i];
            if c & 0x80 == 0x00 {
                // 1 byte character
                result.push(char::from(c & 0x7F));
            } else if c & 0xE0 == 0xC0 {
                // 2 byte character
                let mut c: u32 = 0;
                c |= (value.data[i] as u32) & 0x1F << 6;
                if value.data[i+1] & 0xC0 == 0x80 {
                    c |= (value.data[i] as u32) & 0x3F;
                    result.push(char::from_u32(c).unwrap());
                }
            } else if c & 0xF0 == 0xE0 {
                // 3 byte character
                let mut c: u32 = 0;
                c |= (value.data[i] as u32) & 0x0F << 12;
                if value.data[i+1] & 0xC0 == 0x80 {
                    c |= (value.data[i] as u32) & 0x3F << 6;
                    if value.data[i+2] & 0xC0 == 0x80 {
                        c |= (value.data[i] as u32) & 0x3F;
                        result.push(char::from_u32(c).unwrap());
                    }
                }
            }
        }
        result
    }
}
