use crate::types::{Identifier, UUID};

// id 0x00
pub struct ClientInformation {
    pub locale: String,
    pub view_distance: i8,
    pub chat_mode: i32, // VarInt Enum
    pub chat_colors: bool,
    pub displayed_skin_parts: u8,
    pub main_hand: i32, // VarInt Enum
    pub enable_text_filtering: bool,
    pub allow_server_listings: bool,
}

// id 0x01
pub struct ServerboundPluginMessage {
    pub channel: Identifier,
    pub data: Vec<u8>,
}

// id 0x02
pub struct AcknowledgeFinishConfiguration {}

// id 0x03
pub struct ServerboundKeepAlive {
    pub keep_alive_id: i64,
}

// id 0x04
pub struct Pong {
    pub id: i32, // Int
}

// id 0x05
pub struct ResourcePackResponse {
    pub uuid: UUID,
    pub result: i32, // VarInt Enum
}
