use std::{future::Future, io::Cursor};

use anyhow::Result;
use tokio::io::{AsyncReadExt, AsyncWriteExt};

use crate::{packet::Packet, types::{Read, Write, WriteVarInt}};

// id 0x00
pub struct Handshake {
    pub prot_ver: i32, // VarInt
    pub server_addr: String, // String (255)
    pub server_port: u16, // Unsigned Short
    pub next_state: i32, // VarInt Enum
}

// id 0xFE
pub struct LegacyServerListPing {
    pub payload: u8, // Unsigned Byte
}

impl Handshake {
    pub async fn read<T: AsyncReadExt + Send + Unpin + Read>(stream: &mut T) -> Result<Self> {
        let (prot_ver, _) = stream.p_read_var_int().await?;
        let (server_addr, _) = stream.p_read_string().await?;
        let (server_port, _) = stream.p_read_u16().await?;
        let (next_state, _) = stream.p_read_var_int().await?;
        Ok(Handshake {
            prot_ver,
            server_addr,
            server_port,
            next_state,
        })
    }
    pub async fn write<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Result<()> {
        let mut data = Vec::new();
        0x00.p_write_var_int(&mut data).await?;
        self.prot_ver.p_write_var_int(&mut data).await?;
        self.server_addr.p_write(&mut data).await?;
        self.server_port.p_write(&mut data).await?;
        self.next_state.p_write(&mut data).await?;
        (data.len() as i32).p_write_var_int(stream).await?;
        data.p_write(stream).await?;
        Ok(())
    }
}

impl LegacyServerListPing {
    pub async fn read<T: AsyncReadExt + Send + Unpin + Read>(stream: &mut T) -> Result<Self> {
        let (payload, _) = stream.p_read_u8().await?;
        Ok(LegacyServerListPing {
            payload
        })
    }
    pub async fn write<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Result<()> {
        let mut data = Vec::new();
        0xFE.p_write_var_int(&mut data).await?;
        self.payload.p_write(&mut data).await?;
        (data.len() as i32).p_write_var_int(stream).await?;
        data.p_write(stream).await?;
        Ok(())
    }
}
