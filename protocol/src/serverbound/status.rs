use anyhow::Result;
use tokio::io::{AsyncReadExt, AsyncWriteExt};

use crate::types::{Read, Write, WriteVarInt};

// id 0x00
pub struct StatusRequest {}

// id 0x01
pub struct PingRequest {
    pub payload: i64,
}

impl StatusRequest {
    pub async fn read<T: AsyncReadExt + Send + Unpin + Read>(_stream: &mut T) -> Result<Self> {
        Ok(StatusRequest {})
    }

    pub async fn write<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Result<()> {
        0x00.p_write_var_int(stream).await?;
        0.p_write_var_int(stream).await?;
        Ok(())
    }
}

impl PingRequest {
    pub async fn read<T: AsyncReadExt + Send + Unpin + Read>(stream: &mut T) -> Result<Self> {
        let (payload, _) = stream.p_read_i64().await?;
        Ok(PingRequest { payload })
    }

    pub async fn write<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Result<()> {
        let mut data = Vec::new();
        0x01.p_write_var_int(&mut data).await?;
        self.payload.p_write(&mut data).await?;
        (data.len() as i32).p_write_var_int(stream).await?;
        data.p_write(stream).await?;
        Ok(())
    }
}
