use anyhow::{anyhow, Result};
use tokio::io::{AsyncReadExt, AsyncWriteExt};

use crate::types::{Read, Write, WriteVarInt, UUID};

// id 0x00
#[derive(Debug)]
pub struct LoginStart {
    pub name: String,
    pub player_uuid: UUID,
}
impl LoginStart {
    pub async fn read<T: AsyncReadExt + Send + Unpin + Read>(stream: &mut T) -> Result<Self> {
        let (name, _) = stream.p_read_string().await?;
        let (player_uuid, _) = stream.p_read_uuid().await?;
        Ok(LoginStart { name, player_uuid })
    }
    pub async fn write<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Result<()> {
        let mut data = Vec::new();
        0x00.p_write_var_int(&mut data).await?;
        self.name.p_write(&mut data).await?;
        self.player_uuid.p_write(&mut data).await?;
        (data.len() as i32).p_write_var_int(stream).await?;
        data.p_write(stream).await?;
        Ok(())
    }
}

// id 0x01
#[derive(Debug)]
pub struct EncryptionResponse {
    pub shared_secret: Vec<u8>,
    pub verify_token: Vec<u8>,
}
impl EncryptionResponse {
    pub async fn read<T: AsyncReadExt + Send + Unpin + Read>(stream: &mut T) -> Result<Self> {
        let (shared_secret, _) = stream.p_read_byte_array().await?;
        let (verify_token, _) = stream.p_read_byte_array().await?;
        Ok(EncryptionResponse { shared_secret, verify_token })
    }
    pub async fn write<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Result<()> {
        let mut data = Vec::new();
        0x01.p_write_var_int(&mut data).await?;
        self.shared_secret.p_write(&mut data).await?;
        self.verify_token.p_write(&mut data).await?;
        (data.len() as i32).p_write_var_int(stream).await?;
        data.p_write(stream).await?;
        Ok(())
    }
}

// id 0x02
#[derive(Debug)]
pub struct LoginPluginResponse {
    pub message_id: i32,
    pub successful: bool,
    pub data: Option<Vec<u8>>,
}
impl LoginPluginResponse {
    pub async fn read<T: AsyncReadExt + Send + Unpin + Read>(packet_len: i32, stream: &mut T) -> Result<Self> {
        let (message_id, m_id_len) = stream.p_read_var_int().await?;
        let (successful, successful_len) = stream.p_read_bool().await?;
        let mut data = None;
        if packet_len - m_id_len - successful_len > 0 {
            data = Some(vec![0u8; (packet_len - m_id_len - successful_len) as usize]);
            match data {
                Some(ref mut data) => {
                    stream.read_exact(data).await?;
                }
                None => return Err(anyhow!("Impossible None occured"))
            };
        }
        Ok(LoginPluginResponse { message_id, successful, data })
    }
    pub async fn write<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Result<()> {
        let mut data = Vec::new();
        0x02.p_write_var_int(&mut data).await?;
        self.message_id.p_write_var_int(&mut data).await?;
        self.successful.p_write(&mut data).await?;
        if let Some(d) = self.data {
            d.p_write(&mut data).await?;
        }
        (data.len() as i32).p_write_var_int(stream).await?;
        data.p_write(stream).await?;
        Ok(())
    }
}

// id 0x03
#[derive(Debug)]
pub struct LoginAcknowledged {}
impl LoginAcknowledged {
    pub async fn read<T: AsyncReadExt + Send + Unpin + Read>(_stream: &mut T) -> Result<Self> {
        Ok(LoginAcknowledged {})
    }
    pub async fn write<T: AsyncWriteExt + Send + Unpin>(self, stream: &mut T) -> Result<()> {
        let mut data = Vec::new();
        0x03.p_write_var_int(&mut data).await?;
        (data.len() as i32).p_write_var_int(stream).await?;
        data.p_write(stream).await?;
        Ok(())
    }
}
