use super::block::Block;

pub struct Chunk {
    pub x: i32,
    pub y: i32,
    pub blocks: [[Vec<Block>; 16]; 16]
}
