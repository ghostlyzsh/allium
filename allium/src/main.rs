use std::io;

use client::accept_client;
use server::Server;
use tokio::net::TcpListener;

mod client;
mod server;
mod world;

#[tokio::main]
async fn main() -> io::Result<()> {
    let server = Server {
        max_players: 10,
    };

    // TODO: configurable port
    let listener = TcpListener::bind("127.0.0.1:25565").await?;

    loop {
        let (stream, _) = listener.accept().await?;

        tokio::spawn(async move {
            match accept_client(stream).await {
                Ok(_) => {},
                Err(e) => panic!("{}", e)
            };
        });
    }
}
