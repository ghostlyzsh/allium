use std::io::{Cursor, Write};

use anyhow::{anyhow, Result};
use protocol::{clientbound::{configuration::{FinishConfiguration, RegistryData}, login::{LoginSuccess, Property}, play::{GameEvent, Login}, status::{PingResponse, StatusResponse}}, nbt::Tag, packet::Packet, serverbound::{handshake::Handshake, login::{LoginAcknowledged, LoginStart}, status::{PingRequest, StatusRequest}}, types::{Identifier, Read}};
use tokio::{io::AsyncReadExt, net::TcpStream};

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum ClientState {
    Handshaking,
    Status,
    Login,
    Configuration,
    Play,
}

pub async fn accept_client(mut stream: TcpStream) -> Result<()> {
    // client has been accepted
    // handshake
    let mut state = ClientState::Handshaking;
    let (id, mut data) = stream.p_read_frame().await?;
    if id == 0x00 {
        let handshake = Handshake::read(&mut data).await?;
        state = match handshake.next_state {
            1 => ClientState::Status,
            2 => ClientState::Login,
            _ => { return Err(anyhow!("Invalid next state")) }
        };
        // split into either status or login
        if state == ClientState::Status {
            let (id, mut data) = stream.p_read_frame().await?;
            assert_eq!(id, 0x00);
            let _status_request = StatusRequest::read(&mut data).await?;
            // request received, respond now
            let server_version = "1.20.5";
            let protocol_version = 765;
            let description = "Welcome to Allium!";
            let max_players = 20;
            let online_players = 0;
            let json_response = format!("{{\
                \"version\": {{\
                    \"name\": \"{server_version}\",\
                    \"protocol\": {protocol_version}\
                }},\
                \"players\": {{\
                    \"max\": {max_players},\
                    \"online\": {online_players}\
                }},\
                \"description\": {{\
                    \"text\": \"{description}\"\
                }}\
            }}");
            StatusResponse {
                json_response
            }.write(&mut stream).await.expect("Failed to send StatusResponse");
            // status response sent, now pinging
            let (id, mut data) = stream.p_read_frame().await.expect("Failed to read frame");
            assert_eq!(id, 0x01);
            let ping_request = PingRequest::read(&mut data).await.expect("Failed to read PingRequest");
            PingResponse {
                payload: ping_request.payload,
            }.write(&mut stream).await.expect("Failed to send PingResponse");
        } else if state == ClientState::Login {
            let (id, mut data) = stream.p_read_frame().await.expect("Failed to read frame");
            assert_eq!(id, 0x00);
            let login_start = LoginStart::read(&mut data).await.expect("Failed to read LoginStart");
            LoginSuccess {
                uuid: login_start.player_uuid,
                username: login_start.name,
                properties: vec![],
            }.write(&mut stream).await.expect("Failed to send LoginSuccess");
            let (id, mut data) = stream.p_read_frame().await.expect("Failed to read frame");
            assert_eq!(id, 0x03);
            let _login_acknowledged = LoginAcknowledged::read(&mut data).await.expect("Failed to read LoginAcknowledged");
            state = ClientState::Configuration;
            let mut stream = Cursor::new(Vec::new());
            RegistryData {
                registry_codec: Tag::Compound(None, vec![
                    Tag::Compound(Some("minecraft:worldgen/biome".to_string()), vec![
                        Tag::Str(Some("type".to_string()), "minecraft:worldgen/biome".to_string()),
                        Tag::List(Some("value".to_string()), vec![
                            Tag::Compound(None, vec![
                                Tag::Str(Some("name".to_string()), "minecraft:plains".to_string()),
                                Tag::Int(Some("id".to_string()), 0),
                                Tag::Compound(Some("element".to_string()), vec![
                                    Tag::Byte(Some("has_precipitation".to_string()), 1),
                                    Tag::Float(Some("temperature".to_string()), 1.0),
                                    Tag::Float(Some("downfall".to_string()), 1.0),
                                    Tag::Compound(Some("effects".to_string()), vec![
                                        Tag::Int(Some("fog_color".to_string()), 8364543),
                                        Tag::Int(Some("water_color".to_string()), 8364543),
                                        Tag::Int(Some("water_fog_color".to_string()), 8364543),
                                        Tag::Int(Some("sky_color".to_string()), 8364543),
                                    ]),
                                ]),
                            ])
                        ])
                    ]),
                    Tag::Compound(Some("minecraft:dimension_type".to_string()), vec![
                        Tag::Str(Some("type".to_string()), "minecraft:dimension_type".to_string()),
                        Tag::List(Some("value".to_string()), vec![
                            Tag::Compound(None, vec![
                                Tag::Str(Some("name".to_string()), "minecraft:overworld".to_string()),
                                Tag::Int(Some("id".to_string()), 0),
                                Tag::Compound(Some("element".to_string()), vec![
                                    Tag::Byte(Some("has_skylight".to_string()), 0),
                                    Tag::Byte(Some("has_ceiling".to_string()), 0),
                                    Tag::Byte(Some("ultrawarm".to_string()), 0),
                                    Tag::Byte(Some("natural".to_string()), 1),
                                    Tag::Double(Some("coordinate_scale".to_string()), 1.0),
                                    Tag::Byte(Some("bed_works".to_string()), 1),
                                    Tag::Byte(Some("respawn_anchor_works".to_string()), 0),
                                    Tag::Int(Some("min_y".to_string()), 0),
                                    Tag::Int(Some("height".to_string()), 16),
                                    Tag::Int(Some("logical_height".to_string()), 16),
                                    Tag::Str(Some("infiniburn".to_string()), "#minecraft:netherrack".to_string()),
                                    Tag::Str(Some("effects".to_string()), "minecraft:overworld".to_string()),
                                    Tag::Float(Some("ambient_light".to_string()), 0.0),
                                    Tag::Byte(Some("piglin_safe".to_string()), 1),
                                    Tag::Byte(Some("has_raids".to_string()), 1),
                                    Tag::Int(Some("monster_spawn_light_level".to_string()), 0),
                                    Tag::Int(Some("monster_spawn_block_light_limit".to_string()), 0),
                                ]),
                            ])
                        ])
                    ]),
                ]),
            }.write(&mut stream).await.expect("Failed to send RegistryData");
            FinishConfiguration {}.write(&mut stream).await.expect("Failed to send FinishConfiguration");
            let (mut id, mut _data) = stream.p_read_frame().await.expect("Failed to read frame");
            while id != 0x02 {
                (id, _data) = stream.p_read_frame().await.expect("Failed to read frame");
            }
            assert_eq!(id, 0x02);
            state = ClientState::Play;
            Login {
                entity_id: 0,
                is_hardcore: false,
                dimension_names: vec![Identifier { namespace: "minecraft".to_string(), value: "overworld".to_string() }],
                max_players: 10,
                view_distance: 10,
                simulation_distance: 10,
                reduced_debug_info: false,
                enable_respawn_screen: true,
                do_limited_crafting: false,
                dimension_type: Identifier { namespace: "minecraft".to_string(), value: "overworld".to_string() },
                dimension_name: Identifier { namespace: "minecraft".to_string(), value: "overworld".to_string() },
                hashed_seed: 0,
                game_mode: 1,
                previous_game_mode: -1,
                is_debug: false,
                is_flat: false,
                has_death_location: false,
                death_dimension_name: None,
                death_location: None,
                portal_cooldown: 20,
            }.write(&mut stream).await.expect("Failed to send Login");
            GameEvent {
                event: 13,
                value: 0.0,
            }.write(&mut stream).await.expect("Failed to send GameEvent");
        }
    } else {
        return Err(anyhow!("Unrecognized handshake packet"));
    }
    Ok(())
}
